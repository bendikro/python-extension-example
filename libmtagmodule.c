#include <Python.h>

static PyObject *MTagError;
#include <stdbool.h>

typedef struct {
	PyObject_HEAD;
	PyObject *tag;
} File;

typedef struct {
	PyObject_HEAD;
	PyObject *file;
} Tag;

static PyTypeObject FileType = {
	PyObject_HEAD_INIT(NULL)
};

static PyTypeObject TagType = {
	PyObject_HEAD_INIT(NULL)
};

static void
Tag_dealloc(Tag* self)
{
	self->ob_type->tp_free((PyObject*) self);
}

static int
Tag_init(PyObject *self,
	 PyObject *args,
	 PyObject *kwords)
{
	Tag *c_self;
	PyObject *file;
	const char *tag_id = NULL;
	int create = false;

	c_self = (Tag *) self;

	if (!PyArg_ParseTuple(args, "O|si", &file, &tag_id, &create))
		return -1;

	c_self->file = file;

	return 0;
}

static PyObject *
Tag_set(Tag *self,
	PyObject *args)
{
	const char *key;
	const char *value;

	PyArg_ParseTuple(args, "ss", &key, &value);

	Py_INCREF(Py_None);
	return Py_None;
}

static void
File_dealloc(File* self)
{
	self->ob_type->tp_free((PyObject*) self);
}

static int
File_init(PyObject *self,
	  PyObject *args,
	  PyObject *kwords)
{
	//File *c_self;
	const char *c_file_name;

	//c_self = (File *) self;

	if (!PyArg_ParseTuple(args, "s", &c_file_name))
		return -1;

	/** @todo raise exception. */
#if 0
	if (!c_self->c_file)
#endif

	return 0;
}


static PyObject *
File_save(File *self,
	  PyObject *args)
{

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
File_strip(File *self,
	   PyObject *args)
{
	const char *tag_id = NULL;

	PyArg_ParseTuple(args, "s", &tag_id);

	Py_INCREF(Py_None);
	return Py_None;
}

static PyMethodDef File_methods[] = {
	{ "save", (PyCFunction)File_save, METH_NOARGS, "Save." },
	{ "strip", (PyCFunction)File_strip, METH_VARARGS, "Strip Tag." },
	{ NULL }
};

static PyMethodDef Tag_methods[] = {
	{ "set", (PyCFunction)Tag_set, METH_VARARGS, "Set." },
	{ NULL }
};

static PyMethodDef MTagMethods[] = {
	{ NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC
initlibmtag(void)
{
	PyObject *m;

	m = Py_InitModule("libmtag", MTagMethods);

	TagType.tp_new = PyType_GenericNew;
	TagType.tp_name = "Tag";
	TagType.tp_basicsize = sizeof(Tag);
	TagType.tp_dealloc = (destructor) Tag_dealloc;
	TagType.tp_flags = Py_TPFLAGS_DEFAULT;
	TagType.tp_methods = Tag_methods;
	TagType.tp_init = Tag_init;

	FileType.tp_new = PyType_GenericNew;
	FileType.tp_name = "File";
	FileType.tp_basicsize = sizeof(File);
	FileType.tp_dealloc = (destructor) File_dealloc;
	FileType.tp_flags = Py_TPFLAGS_DEFAULT;
	FileType.tp_methods = File_methods;
	FileType.tp_init = File_init;

	if (PyType_Ready(&FileType) < 0)
		return;

	if (PyType_Ready(&TagType) < 0)
		return;

	PyModule_AddObject(m, "Tag", (PyObject *) &TagType);
	PyModule_AddObject(m, "File", (PyObject *) &FileType);

	MTagError = PyErr_NewException("libmtag.error", NULL, NULL);
	Py_INCREF(MTagError);
	PyModule_AddObject(m, "error", MTagError);
}
